﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Restaurant.Models
{
    public partial class hcirestaurantsContext : DbContext
    {
        public hcirestaurantsContext()
        {
        }

        public hcirestaurantsContext(DbContextOptions<hcirestaurantsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cities> Cities { get; set; }
        public virtual DbSet<Covid19> Covid19 { get; set; }
        public virtual DbSet<Cuisines> Cuisines { get; set; }
        public virtual DbSet<Restaurants> Restaurants { get; set; }
        public virtual DbSet<Reviews> Reviews { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=localhost;Database=hci-restaurants;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cities>(entity =>
            {
                entity.ToTable("cities");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CityName)
                    .HasColumnName("city_name")
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.Property(e => e.CountryName)
                    .HasColumnName("country_name")
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .HasColumnName("full_name")
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.Property(e => e.StateCode)
                    .HasColumnName("state_code")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasColumnName("state_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Covid19>(entity =>
            {
                entity.ToTable("covid19");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Comments)
                    .HasColumnName("comments")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Curbside).HasColumnName("curbside");

                entity.Property(e => e.IndoorDining).HasColumnName("indoor_dining");

                entity.Property(e => e.LimitSeating).HasColumnName("limit_seating");

                entity.Property(e => e.RestaurantId).HasColumnName("restaurant_id");

                entity.Property(e => e.TakeOut).HasColumnName("take_out");

                entity.HasOne(d => d.Restaurant)
                    .WithMany(p => p.Covid19)
                    .HasForeignKey(d => d.RestaurantId)
                    .HasConstraintName("FK__covid19__restaur__52593CB8");
            });

            modelBuilder.Entity<Cuisines>(entity =>
            {
                entity.ToTable("cuisines");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CuisineName)
                    .HasColumnName("cuisine_name")
                    .HasMaxLength(70)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Restaurants>(entity =>
            {
                entity.ToTable("restaurants");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AggregateRating)
                    .HasColumnName("aggregate_rating")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CityId).HasColumnName("city_id");

                entity.Property(e => e.CuisineId).HasColumnName("cuisine_id");

                entity.Property(e => e.Cuisines)
                    .HasColumnName("cuisines")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Establishment)
                    .HasColumnName("establishment")
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.HasDelivery).HasColumnName("has_delivery");

                entity.Property(e => e.HasTakeaway).HasColumnName("has_takeaway");

                entity.Property(e => e.Locality)
                    .HasColumnName("locality")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.LocalityVerbose)
                    .HasColumnName("locality_verbose")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MenuUrl)
                    .HasColumnName("menu_url")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PriceRange).HasColumnName("price_range");

                entity.Property(e => e.RatingText)
                    .HasColumnName("rating_text")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateCode)
                    .HasColumnName("state_code")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Timings)
                    .HasColumnName("timings")
                    .HasMaxLength(700)
                    .IsUnicode(false);

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCode)
                    .HasColumnName("zip_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.CityNavigation)
                    .WithMany(p => p.Restaurants)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("City ID");

                entity.HasOne(d => d.Cuisine)
                    .WithMany(p => p.Restaurants)
                    .HasForeignKey(d => d.CuisineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__restauran__cuisi__4F7CD00D");
            });

            modelBuilder.Entity<Reviews>(entity =>
            {
                entity.ToTable("reviews");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CustomerName)
                    .HasColumnName("customer_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rating).HasColumnName("rating");

                entity.Property(e => e.RatingText)
                    .HasColumnName("rating_text")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RestaurantId).HasColumnName("restaurant_id");

                entity.Property(e => e.ReviewText)
                    .HasColumnName("review_text")
                    .IsUnicode(false);

                entity.Property(e => e.ReviewTimeFriendly)
                    .HasColumnName("review_time_friendly")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Restaurant)
                    .WithMany(p => p.Reviews)
                    .HasForeignKey(d => d.RestaurantId)
                    .HasConstraintName("FK__reviews__restaur__5535A963");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
