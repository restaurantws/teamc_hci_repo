﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Restaurant.Models
{
    public partial class Covid19
    {
        public int Id { get; set; }
        public int? RestaurantId { get; set; }
        [DisplayName("Take Out")]
        public bool? TakeOut { get; set; }
        [DisplayName("Limited Seating")]
        public bool? LimitSeating { get; set; }
        [DisplayName("Indoor Dining")]
        public bool? IndoorDining { get; set; }
        [DisplayName("Curbside")]
        public bool? Curbside { get; set; }
        [DisplayName("Coments")]
        public string Comments { get; set; }

        public virtual Restaurants Restaurant { get; set; }
    }
}
