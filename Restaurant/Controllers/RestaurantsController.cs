﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Restaurant.Models;

namespace Restaurant.Controllers
{
    public class RestaurantsController : Controller
    {
        private readonly hcirestaurantsContext _context;

        public RestaurantsController(hcirestaurantsContext context)
        {
            _context = context;
        }

        // GET: Restaurants
        public async Task<IActionResult> Index(string sortOrder, string searchString, string searchCusines)
        {
            
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Name" : "";
            ViewData["RateSortParm"] = String.IsNullOrEmpty(sortOrder) ? "AggregateRating" : "";
            ViewData["CuisineSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Cuisines" : "";
            ViewData["CurrentFilter"] = searchString;
            ViewData["CusinesFilter"] = searchCusines;

            var restraunts = from s in _context.Restaurants
                             select s;
            var cusines = from c in _context.Cuisines
                          select c;
            if (!String.IsNullOrEmpty(searchString))
            {
                restraunts = restraunts.Where(s => s.Name.Contains(searchString));
                
            }
            if (!String.IsNullOrEmpty(searchCusines))
            {
                restraunts = restraunts.Where(s => s.Cuisines.Contains(searchCusines));
            }
            switch (sortOrder)
            {
                case "Name":
                    restraunts = restraunts.OrderBy(s => s.Name);
                    break;
                case "AggregateRating":
                    restraunts = restraunts.OrderByDescending(s => s.AggregateRating);
                    break;
                case "Cuisines":
                    restraunts = restraunts.OrderBy(s => s.Cuisines);
                    break;
            }
            
            return View(await restraunts.AsNoTracking().ToListAsync());
        }

        // GET: Restaurants/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurants = await _context.Restaurants
                .Include(r => r.CityNavigation)
                .Include(r => r.Cuisine)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (restaurants == null)
            {
                return NotFound();
            }

            return View(restaurants);
        }

        // GET: Restaurants/Create
        public IActionResult Create()
        {
            ViewData["CityId"] = new SelectList(_context.Cities, "Id", "Id");
            ViewData["CuisineId"] = new SelectList(_context.Cuisines, "Id", "Id");
            return View();
        }

        // POST: Restaurants/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CityId,CuisineId,Cuisines,Currency,Establishment,HasDelivery,HasTakeaway,Address,City,StateCode,Locality,LocalityVerbose,ZipCode,MenuUrl,Name,Telephone,PriceRange,Timings,Url,AggregateRating,RatingText")] Restaurants restaurants)
        {
          
            if (ModelState.IsValid)
            {
               
                _context.Add(restaurants);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CityId"] = new SelectList(_context.Cities, "Id", "Id", restaurants.CityId);
            ViewData["CuisineId"] = new SelectList(_context.Cuisines, "Id", "Id", restaurants.CuisineId);
            return View(restaurants);
        }

        // GET: Restaurants/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurants = await _context.Restaurants.FindAsync(id);
            if (restaurants == null)
            {
                return NotFound();
            }
            ViewData["CityId"] = new SelectList(_context.Cities, "Id", "Id", restaurants.CityId);
            ViewData["CuisineId"] = new SelectList(_context.Cuisines, "Id", "Id", restaurants.CuisineId);
            return View(restaurants);
        }

        // POST: Restaurants/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CityId,CuisineId,Cuisines,Currency,Establishment,HasDelivery,HasTakeaway,Address,City,StateCode,Locality,LocalityVerbose,ZipCode,MenuUrl,Name,Telephone,PriceRange,Timings,Url,AggregateRating,RatingText")] Restaurants restaurants)
        {
            if (id != restaurants.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(restaurants);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RestaurantsExists(restaurants.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CityId"] = new SelectList(_context.Cities, "Id", "Id", restaurants.CityId);
            ViewData["CuisineId"] = new SelectList(_context.Cuisines, "Id", "Id", restaurants.CuisineId);
            return View(restaurants);
        }

        // GET: Restaurants/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurants = await _context.Restaurants
                .Include(r => r.CityNavigation)
                .Include(r => r.Cuisine)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (restaurants == null)
            {
                return NotFound();
            }

            return View(restaurants);
        }

        // POST: Restaurants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var restaurants = await _context.Restaurants.FindAsync(id);
            _context.Restaurants.Remove(restaurants);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RestaurantsExists(int id)
        {
            return _context.Restaurants.Any(e => e.Id == id);
        }
    }
}
